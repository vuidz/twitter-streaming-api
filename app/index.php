<?php
require 'config.php';
require_once('../lib/Phirehose.php');
require_once('../lib/UserstreamPhirehose.php');
require_once('../lib3rd/Color.php');
require_once('../lib3rd/TwitterUpload.php');
require __DIR__ . '/../vendor/autoload.php';

/**
 * Example of using Phirehose to display the 'sample' twitter stream.
 * @property TwitterUpload $uploadHelper;
 * @property \Codebird\Codebird $twitter;
 */
class SampleConsumer extends UserstreamPhirehose
{
    private $trackTweet = [];
    private $trackHashTag = [];

    private $listFollower = [];
    private $listRetweet = [];
    private $listTweet = [];

    private $listWinner = []; // list user valid

    private $listUserExtraInfo = []; // Extra info of user

    // Printing
    private $printHeler;

    public function __construct($username, $password, $method = self::METHOD_USER, $format = self::FORMAT_JSON, $lang = FALSE)
    {
        parent::__construct($username, $password, $method, $format, $lang);
        /**
         * Auth For Twitter
         *
         * How to mapping method
         * https://www.jublo.net/projects/codebird/php#mapping-api-methods-to-codebird-function-calls
         */
        \Codebird\Codebird::setConsumerKey(CONSUMER_KEY, CONSUMER_SECRET);
        $this->twitter = \Codebird\Codebird::getInstance();
        $this->twitter->setToken(ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
        $this->twitter->setReturnFormat(CODEBIRD_RETURNFORMAT_ARRAY); // don't worries about this

        // Setting Timeout
        $this->twitter->setRemoteDownloadTimeout(60*1000); // 1"
        $this->twitter->setTimeout(10*60*1000); // 1"
        $this->twitter->setConnectionTimeout(10*60*1000); // 10s

        // Merge list follow
        $followerList = $this->twitter->followers_ids();
        $this->listFollower = array_merge($this->listFollower, $followerList['ids']);

        // For debug only
        $this->printHeler = new Colors();

        // Testing function
        $this->testUploadFile();
    }

    /**
     * This Main Streaming !!! Importance
     *
     * @param string $status
     */
    public function enqueueStatus($status)
    {
        $this->filterEvent($status, $isOk)->checkWinner($isOk);
    }

    public function filterEvent($status, &$isOk)
    {
        $status = json_decode($status);

        if (($status->event ?? '') === 'follow') {
            $this->debug('follow Event');
            $this->listFollower[] = $status->source->id;
            $this->listUserExtraInfo[$status->source->id] = $status->source;
            $isOk = true;
        } else if (isset($status->retweeted_status)) {
            $idTweet = $status->retweeted_status->id;
            if (in_array($idTweet, $this->trackTweet)) {
                $this->debug('retweeted_status Event');
                $this->listRetweet[] = $status->user->id;
                $this->listUserExtraInfo[$status->user->id] = $status->user;

                $isOk = true;
            }
        } else if (isset($status->entities) && isset($status->entities->hashtags)) {
            $statusHashTag = array_map(function ($item) {
                return $item->text;
            }, $status->entities->hashtags);

            $issetHashtag = array_intersect($statusHashTag, $this->trackHashTag);
            if (count($issetHashtag) > 0) {
                $this->debug('Tweet Event');
                $this->listTweet[] = $status->user->id;
                $this->listUserExtraInfo[$status->user->id] = $status->user;

                $isOk = true;
            }
        }

        if ($isOk) {
            // Make sure all list user unique
            $this->listFollower = array_unique($this->listFollower);
            $this->listTweet = array_unique($this->listTweet);
            $this->listRetweet = array_unique($this->listRetweet);
        }

        return $this;
    }

    public function setTrackTweet($track)
    {
        $track = (array)$track;
        $this->trackTweet = array_merge($this->listRetweet, $track);
    }

    public function setTrack(array $trackWords)
    {
        parent::setTrack($trackWords);
        $this->trackHashTag = array_map(function ($item) {
            return str_replace('#', '', $item);
        }, $this->trackWords);
    }

    /**
     * Demo Update Status With Status
     */
    private function testUploadFile() {

        $this->uploadHelper = new TwitterUpload($this->twitter);
        // Test upload
        $mediaId = $this->uploadHelper->uploadPerform('../medias/image.gif');
        $this->twitter->statuses_update([
            "status" => "Test update tweet with media !!!!",
            "media_ids" => $mediaId
        ]);

        exit;
    }

    private function checkWinner($isOk)
    {
        if ($isOk) {
            $listTemp = array_intersect(
                $this->listFollower,
                $this->listRetweet,
                $this->listTweet
            );

            $listTemp = array_values($listTemp);

            $this->debug(json_encode([
                "listFollower" => $this->listFollower,
                "listRetweet" => $this->listRetweet,
                "listTweet" => $this->listTweet,
                "listIntersect" => $listTemp,
            ], JSON_PRETTY_PRINT));

            if (count($listTemp) > 0) {
                $listSend = array_diff($listTemp, $this->listWinner);
                $this->sendingPrize($listSend);
                // New List
                $this->listWinner = $listTemp;
            }
        }
    }

    private function sendingPrize($listSend)
    {
        $this->debug('BEGIN SENDING PRIZE');
        foreach ($listSend as $userId) {
            $userInfo = $this->listUserExtraInfo[$userId];

            $this->debug(["userInfo" => $userInfo]);

            // Sending Direct Message
            $this->twitter->directMessages_new([
                "user_id" => $userInfo->id, // Optional
                "screen_name" => $userInfo->screen_name, // Optional
                "text" => "@" . $userInfo->screen_name . " congratulation you so lucky !!!!",
            ]);

            // Update Status
            $this->twitter->statuses_update([
                "status" => "@" . $userInfo->screen_name . " congratulation you so lucky !!!!",
            ]);
        }
    }

    private function debug(...$strs)
    {
        foreach ($strs as $str) {
            $this->printHeler->print($str);
        }
    }
}

/**
 * This Define Using For Twitter Streaming
 */
define("TWITTER_CONSUMER_KEY", CONSUMER_KEY);
define("TWITTER_CONSUMER_SECRET", CONSUMER_SECRET);
define("OAUTH_TOKEN", ACCESS_TOKEN);
define("OAUTH_SECRET", ACCESS_TOKEN_SECRET);

/**
 * Start Streaming
 */
$sc = new SampleConsumer(OAUTH_TOKEN, OAUTH_SECRET, Phirehose::METHOD_USER);
$sc->setTrack(['#hoayeuthuong', '#hoatinhyeu', '#trackhoa', '#hoathegioi']);
$sc->setTrackTweet(['928178256129376257']);
$sc->consume();
