<?php

/**
 * Class TwitterUpload
 *
 * @property Codebird\Codebird $twitter
 */
class TwitterUpload
{
    private $filePath;

    private $fileName;
    private $fileSize;
    private $mediaIds;

    public $fileInfo;

    function __construct(Codebird\Codebird $twitter)
    {
        $this->twitter = $twitter;
        return $this;
    }

    /**
     * @param $filePath
     * @return mixed
     *
     * @see https://www.jublo.net/projects/codebird/php#uploading-images-and-videos
     */
    function uploadPerform($filePath)
    {
        // Init
        $this->filePath = $filePath;
        $this->fileName = basename($filePath);
        $this->fileSize = filesize($filePath);

        $this->uploadInit();
        $this->uploadAppend();
        $this->uploadFinalize();

        return $this->mediaIds;
    }

    /*
     * Using this function after perform upload to get more
     * detail of file
     */
    public function getFileInfo()
    {
        return [
            'file_size' => $this->fileSize,
            'file_name' => $this->fileName,
            'file_mime' => mime_content_type($this->filePath),
            'media_category' => $this->getTweetCategory($this->filePath),
            // This result after perform INIT command
            'file_detail_twitter' => $this->fileInfo
        ];
    }

    private function uploadInit()
    {
        $dataRequest = [
            "command" => "INIT",
            "media_type" => mime_content_type($this->filePath),
            "total_bytes" => $this->fileSize
        ];

        if ($this->getTweetCategory($this->filePath) === '') {
            die('Error File Type Not Support');
        }

        $dataRequest['media_category'] = $this->getTweetCategory($this->filePath);

        $res = $this->twitter->media_upload($dataRequest);

        $this->mediaIds = $res['media_id'];
        // After request hold this result
        $this->fileInfo = $res;

        $this->debug($dataRequest);
        $this->debug($res);
    }

    private function uploadAppend()
    {
        $this->debug('APPEND');
        $segmentId = 0;
        $file = fopen($this->filePath, "r");

        while (!feof($file)) {
            $chunk = fread($file, 2 * 1024 * 1024);

            $dataRequest = [
                "command" => "APPEND",
                "segment_index" => $segmentId,
                "media_id" => $this->mediaIds,
                "media" => $chunk,
            ];

            $res = $this->twitter->media_upload($dataRequest);

            if ($res['httpstatus'] < 200 || $res['httpstatus'] > 299) {
                $this->debug($res);
                die();
            }

            $segmentId++;
            $bytesSent = ftell($file);
            $this->debug("%s of %s bytes uploaded", [
                $bytesSent,
                $this->fileSize
            ]);
        }
        fclose($file);
        $this->debug('Upload chunks complete.');
    }

    private function uploadFinalize()
    {
        print('FINALIZE');

        $dataRequest = [
            "command" => "FINALIZE",
            "media_id" => $this->mediaIds,
        ];

        $res = $this->twitter->media_upload($dataRequest);

        if ($res['httpstatus'] < 200 || $res['httpstatus'] > 299) {
            $this->debug($res);
            die();
        }

        $this->checkStatus($res);
    }

    /*
     * Read More https://dev.twitter.com/web/sign-inhttps://dev.twitter.com/rest/reference/get/media/upload-status
     */
    private function checkStatus($res)
    {
        $this->debug('CHECK STATUS');

        $state = $res['processing_info']['state'] ?? null;

        if ($state === 'failed') {
            $this->debug($res);
            die();
        }

        if ($state === 'succeeded' || $state === null) {
            return;
        }

        $timeRemaning = $res['processing_info']['check_after_secs'];
        $this->debug("Video Progress %s", [$state]);
        $this->debug("Video Checking after %s seconds", [$timeRemaning]);

        sleep($timeRemaning);

        $dataRequest = [
            "command" => "STATUS",
            "media_id" => $this->mediaIds,
        ];

        $newRes = $this->twitter->media_upload($dataRequest);
        $this->checkStatus($newRes);
    }

    private function getTweetCategory($filename)
    {
        $mimeType = mime_content_type($filename);
        /**
         * tweet_image
         * tweet_gif (animated GIF only, static not supported)
         * tweet_video
         */
        switch ($mimeType) {
            case 'image/png':
            case 'image/jpeg':
            case 'image/bmp':
                return 'tweet_image';
            case 'image/gif':
                return 'tweet_gif';
            case 'video/mp4':
                return 'tweet_video';
            default:
                return '';
        }
    }

    private function debug($str, $arrs = [], $isDebugMode = true)
    {
        if ($isDebugMode) {
            if (is_array($str) || is_object($str)) {
                $str = json_encode($str);
            }
            if (!empty($arrs)) {
                $str = vsprintf($str, $arrs);
            }
            print ("\n" . $str . "\n");
        }
    }
}
